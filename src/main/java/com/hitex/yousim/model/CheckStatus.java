package com.hitex.yousim.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
public class CheckStatus extends BaseEntity {
    private int time;
}
