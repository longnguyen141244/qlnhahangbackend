package com.hitex.yousim.dto.response.ban;

import com.hitex.yousim.dto.response.IResponseData;
import com.hitex.yousim.model.Ban;
import lombok.Data;

import java.util.List;

@Data
public class BanExt extends Ban implements IResponseData {
    private int status;
}
