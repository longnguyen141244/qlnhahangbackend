package com.hitex.yousim.repository;

import com.hitex.yousim.model.DatBan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface DatBanRepository extends JpaRepository<DatBan, Integer> {
    @Query("SELECT db FROM DatBan db,BanDaDat bdd WHERE db.tblBanDaDatId = bdd.id AND bdd.ngayNhanBan >= ?1 AND bdd.ngayNhanBan <= ?2")
    List<DatBan> getListDatBan(Date startDate, Date endDate);
    @Query("SELECT m FROM DatBan m WHERE m.id = ?1")
    DatBan getById(int id);
    @Query(value = "SELECT db.* FROM tbldatban AS db " +
            "LEFT JOIN tblbandadat AS bdd ON bdd.id = db.tblBanDaDatId " +
            "WHERE (time = ?1 OR time = ?2) AND ngaydat = ?3 AND bdd.tblBanId = ?4 LIMIT 1",
            nativeQuery = true
    )
    DatBan checkStatus(int time1, int time2, String ngaydat, int banId);
}