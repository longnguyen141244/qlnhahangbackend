package com.hitex.yousim.service.impl;

import com.hitex.yousim.dto.request.BaseRequestData;
import com.hitex.yousim.dto.request.ban.BanReq;
import com.hitex.yousim.dto.response.ban.BanExt;
import com.hitex.yousim.dto.response.ban.BanRes;
import com.hitex.yousim.model.Ban;
import com.hitex.yousim.model.BanDaDat;
import com.hitex.yousim.model.CheckStatus;
import com.hitex.yousim.model.DatBan;
import com.hitex.yousim.repository.BanDaDatRepository;
import com.hitex.yousim.repository.BanRepository;
import com.hitex.yousim.repository.DatBanRepository;
import com.hitex.yousim.repository.UserRepository;
import com.hitex.yousim.service.BanService;
import com.hitex.yousim.utils.exception.ApplicationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
@Service
public class BanServiceImpl implements BanService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    BanRepository banRepository;
    @Autowired
    DatBanRepository datBanRepository;

    @Override
    public BanRes danhSachBan(BaseRequestData baseRequestData) throws ApplicationException {
        BanRes banRes = new BanRes();
        BanReq banReq = (BanReq) baseRequestData.getWsRequest();
        try {
            List<Ban> banList = banRepository.getListBan();
            List<BanExt> listExt = new ArrayList<>();

            for(Ban ban : banList) {
                BanExt banExt = new BanExt();
                BeanUtils.copyProperties(ban, banExt);
                DatBan c = datBanRepository.checkStatus(banReq.getTime(), banReq.getTime()-1, banReq.getNgaydat(), ban.getId());
                if(ObjectUtils.isEmpty(c)) {
                    banExt.setStatus(1);
                } else {
                    banExt.setStatus(-1);
                }
                listExt.add(banExt);
            }

            banRes.setBanList(listExt);
        } catch (Exception e) {
            throw new ApplicationException("error");
        }
        return banRes;
    }
}
